
# monai

The framework is divided into a few large modules:

* **application**: Contains the `NetworkManager` type, monitors, and utilities

* **data**: Contains the data stream definition types and example datasets

* **networks**: Contains network definitions, component definitions, and Pytorch specific utilities

* **test**: Contains the unit tests for the framework

* **utils**: Contains a set of utility files for doing namespace aliasing, auto module loading, and other facilities
