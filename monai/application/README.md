
# Application

Contains the code for managing the training process. This includes the `NetworkManager` and its subclasses, monitors,
and other facilities.

* **config**: This has a few facilities for configuration and diagnostic output.

* **metrics**: Defines metric tracking types.

* **handlers**: Defines handlers for implementing functionality at various stages in the training process.

* **engine**: Eventually will have Engine-derived classes for extending Ignite behaviour.
